All Clear Plumbing provides quality plumbing repairs including water heaters, sewer lines, water lines, faucets, toilets, and drain cleaning. They are a family owned and operated company serving the Greenville and Anderson areas of the upstate, South Carolina. Emergency service available.

Address: 18 Piedmont Hwy, Piedmont, SC 29673, USA

Phone: 864-979-7059